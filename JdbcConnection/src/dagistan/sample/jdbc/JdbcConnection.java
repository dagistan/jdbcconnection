package dagistan.sample.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//POSTGRESQL database example
//Please add postgresql-9.2-1002-jdbc4.jar to your Build path. you can find it via google search :)

/*

Postgresql Book table is used. You have to modify your "databasename", "databaseuser" and "databasepassword" fields in code below...

"Book" table:
--
id : Integer
serial : character varying
ip : character varying

 */
public class JdbcConnection {

	public static void main(String args[]) throws SQLException {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/databasename", "databaseuser", "databasepassword");
			c.setAutoCommit(false);
			System.out.println("Database connection is OK.");

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Book;");
			while (rs.next()) {
				int id = rs.getInt("id");
				String serial = rs.getString("serial");
				String ip = rs.getString("ip");

				System.out.println("ID = " + id);
				System.out.println("SERIAL = " + serial);
				System.out.println("IP = " + ip);
				System.out.println();
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		} finally {
			c.close();
			stmt.close();
		}
		System.out.println("SELECTion done successfully.");
	}

}
