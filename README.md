# README #

Run as Java Application.

**Note:** Please use the right .jar and modify the database connection arguments correctly.

### Used Technologies for all projects ###

* Java SE,
* Java EE,
* 3rd party Java frameworks / libraries.

### Requirements ###

* Eclipse IDE
* JDK 1.8
* PostgreSql 9.x
* Postgresql jdbc driver named: postgresql-9.2-1002-jdbc4.jar
* Deployment and Run instructions are included in related project class files. Read carefully.

I hope you enjoy :)